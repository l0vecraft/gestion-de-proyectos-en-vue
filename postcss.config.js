module.exports = {
  purge: {
    enabled: true,
    content: ["./src/**/*.html", "./src/**/*.vue"]
  },
  plugins: [
    // ...
    require("tailwindcss"),
    require("autoprefixer")
    // ...
  ]
};
