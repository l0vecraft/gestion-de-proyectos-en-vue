import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home";
import Formulario from "../views/Formulario";
import Pagina1 from "../views/Pagina1";
import Pagina2 from "../views/Pagina2";
import Blog from "../views/Blog";
import Error404 from "../views/Error404";

Vue.use(VueRouter);

const routes = [
  {
    path: "/home",
    name: "Home",
    component: Home
  },
  {
    path: "/",
    component: Home
  },
  {
    path: "/blog",
    component: Blog
  },
  {
    path: "/pagina1",
    component: Pagina1
  },
  {
    path: "/pagina2",
    component: Pagina2
  },
  {
    path: "/formulario",
    component: Formulario
  },
  {
    path: "*",
    component: Error404
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
