module.exports = {
  theme: {
    fontFamily: "Proxima Nova",
    container: {
      center: true
    },
    extend: {
      colors: {
        vue_green: "#01ff00"
      }
    }
  },
  variants: {},
  extend: {},
  plugins: [require("tailwindcss"), require("autoprefixer")]
};
